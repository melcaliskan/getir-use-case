const mongoose = require('mongoose');
const httpStatus = require('http-status');

/**
 * Records Schema
 * @private
 */
const recordsSchema = new mongoose.Schema({
  key: {
    type: String,
    required: true,
    unique: true,
    trim: true,
  },
  counts: [
    {
      type: Number,
    },
  ],
  value: {
    type: String,
    required: true,
    index: true,
    trim: true,
  },
}, {
  timestamps: true,
});

recordsSchema.statics = {
  /**
   * Get Records via POST request
   *
   * @param {Date} startDate
   * @param {Date} endDate
   * @param {Number} minCount
   * @param {Number} maxCount
   * @returns {Promise<Record, APIError>}
   */
  // eslint-disable-next-line consistent-return
  async list(body) {
    const {
      startDate, endDate, minCount, maxCount,
    } = body;

    try {
      let records;
      if (body) {
        records = await this.aggregate([
          {
            $unwind: {
              path: '$counts',
            },
          },
          {
            $match: {
              createdAt: { $gte: new Date((startDate)), $lte: new Date((endDate)) },
            },
          },
          {
            $group: {
              _id: '$key',
              count: { $sum: '$counts' },
              createdAt: { $first: '$createdAt' },
            },
          },
          {
            $match: {
              count: { $gte: parseInt(minCount, 0), $lt: parseInt(maxCount, 0) },
            },
          },
          {
            $sort: {
              count: 1,
            },
          },
          {
            $project: {
              _id: 0,
              key: '$_id',
              createdAt: '$createdAt',
              totalCount: '$count',
            },
          },
        ]);

        if (records.length > 0) {
          return {
            code: 0,
            msg: 'Success',
            records,
          };
        }

        return {
          code: 1,
          msg: 'Records Not Found',
          status: httpStatus.NOT_FOUND,
        };
      }
    } catch (error) {
      throw new Error({
        code: 2,
        msg: error.msg,
        status: httpStatus.INTERNAL_SERVER_ERROR,
      });
    }
  },
};
/**
 * @typedef Records
 */
module.exports = mongoose.model('Records', recordsSchema);
