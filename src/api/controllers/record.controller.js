const Record = require('../models/record.model');

/**
 * Get Records list with given params
 * @public
 */
exports.list = async (req, res, next) => {
  try {
    const result = await Record.list(req.body);
    res.json(result);
  } catch (error) {
    next(error);
  }
};
