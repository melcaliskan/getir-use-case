const axios = require('axios');
const { expect } = require('chai');

describe('POST /record with Invalid Type of Date Field', () => {
  it('return records with code 0 which means Status 400', async () => {
    const apiQuery = {
      startDate: 'not-a-date',
      endDate: '2015-06-31',
      minCount: 0,
      maxCount: 100,
    };
    await axios.post(`${'http://localhost'}:${process.env.PORT}/record`, apiQuery).then(() => {})
      .catch((error) => {
        expect(error.response.status).equal(422);
        expect(error.response.data).contains({
          code: 2,
          msg: 'Validation Error',
        });
      });
  });
});

describe('POST /record with Invalid Type of Number Field', () => {
  it('return records with Code 1 which means Status 400', async () => {
    const apiQuery = {
      startDate: '2015-05-31',
      endDate: '2015-06-31',
      minCount: 'number',
      maxCount: 100,
    };
    await axios.post(`${'http://localhost'}:${process.env.PORT}/record`, apiQuery).then(() => {})
      .catch((error) => {
        expect(error.response.status).equal(422);
        expect(error.response.data).contains({
          code: 2,
          msg: 'Validation Error',
        });
      });
  });
});

describe('POST /record with Negative Number Field', () => {
  it('return records with Code 1 which means Status 400', async () => {
    const apiQuery = {
      startDate: '2015-05-31',
      endDate: '2015-06-31',
      minCount: -1,
      maxCount: 100,
    };
    await axios.post(`${'http://localhost'}:${process.env.PORT}/record`, apiQuery).then(() => {})
      .catch((error) => {
        expect(error.response.status).equal(422);
        expect(error.response.data).contains({
          code: 2,
          msg: 'Validation Error',
        });
      });
  });
});
