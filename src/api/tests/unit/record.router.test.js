const axios = require('axios');
const { expect } = require('chai');

describe('GET /status', () => {
  it('should return Status 200 (Success) OK', async () => {
    const result = await axios.get(`${'http://localhost'}:${process.env.PORT}/status`);

    expect(result.status).equal(200);
    expect(result.data).equal('OK');
  });
});

describe('GET /docs', () => {
  it('should return Status 200 (Success) for docs', async () => {
    const result = await axios.get(`${'http://localhost'}:${process.env.PORT}/docs`);

    expect('Content-Type', 'text/html; charset=UTF-8');
    expect(result.status).equal(200);
  });
});
