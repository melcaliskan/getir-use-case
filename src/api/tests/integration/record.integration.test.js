const axios = require('axios');
const { expect } = require('chai');

describe('POST /record', () => {
  it('return records with code 0 which means Status 200 (Success)', async () => {
    const apiQuery = {
      startDate: '2015-05-31',
      endDate: '2015-06-31',
      minCount: 100,
      maxCount: 30000,
    };

    const result = await axios.post(`${'http://localhost:'}${process.env.PORT}/record`, apiQuery);

    expect(result.status).equal(200);

    expect(result.data).contain({
      code: 0,
      msg: 'Success',
    });
  });
});
