const express = require('express');
const recordRoutes = require('./record.route');

const router = express.Router();

/**
 * GET /status for health-check
 */
router.get('/status', (req, res) => res.send('OK'));

/**
 * GET /docs for docs
 */
router.use('/docs', express.static('docs'));

router.use('/record', recordRoutes);

module.exports = router;
