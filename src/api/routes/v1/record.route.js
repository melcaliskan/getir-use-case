const express = require('express');
const validate = require('express-validation');
const controller = require('../../controllers/record.controller');
const { records } = require('../../validations/record.validation');

const router = express.Router();

router
  .route('/')
  /**
   * @api {post} /record Get Records
   * @apiDescription Get Records between given dates and counts
   * @apiVersion 1.0.0
   * @apiName Get Records
   * @apiGroup Records
   *
   * @apiParam  {Date}                startDate     Record Start Date
   * @apiParam  {Date}                endDate       Record End Date
   * @apiParam  {Number}              minCount      Min Count
   * @apiParam  {Number}              maxCount      Max Count
   *
   * @apiSuccess (Success 200) {Number}  code         Status Code
   * @apiSuccess (Success 200) {String}  msg          Status Message
   * @apiSuccess (Success 200) {Object[]}  records       Records Result
   *
   * @apiError (Bad Request 400)   ValidationError  Some parameters may contain invalid values
   * @apiError (Forbidden 404)     Forbidden        Records does not exist
   */
  .post(validate(records), controller.list);

module.exports = router;
