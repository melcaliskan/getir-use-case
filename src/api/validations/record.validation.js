const Joi = require('joi');

module.exports = {

  // POST /record
  records: {
    body: {
      startDate: Joi.date(),
      endDate: Joi.date(),
      minCount: Joi.number()
        .integer()
        .allow(0)
        .positive()
        .required(),
      maxCount: Joi.number()
        .integer()
        .positive()
        .allow(0)
        .required(),
    },
  },
};
