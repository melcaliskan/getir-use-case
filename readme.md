# Getir Case Study

Rest API application which is built with Express.js and Mocha(test) and MongoDB.

## Test URL

```
https://getir-use-case.herokuapp.com/
```

## Installation

```
yarn install
```

## Run The Application

If app run with production .env config to start:
```
yarn start
```

If app run with test .env config to start:

```
yarn run dev
```

## Endpoints

### GET /status
```
https://getir-use-case.herokuapp.com/status
```
Health Check endpoint

### GET /docs
```
https://getir-use-case.herokuapp.com/docs
```
To show Request and Response Schemes

### POST /record
Fetch data from MongoDB Collection and return results within queries

## Test

Mocha used for testing phase.

If you want to run only Integration test(s):

```shell
yarn run test:integration
```

If you want to run only Unit test(s):

```shell
yarn run test:unit
```

If you want to run Integration and Unit tests both:

```shell
yarn run test
```